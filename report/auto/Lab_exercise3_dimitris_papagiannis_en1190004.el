(TeX-add-style-hook
 "Lab_exercise3_dimitris_papagiannis_en1190004"
 (lambda ()
   (setq TeX-command-extra-options
         "-output-directory=build")
   (TeX-run-style-hooks
    "uoa_template"
    "soul")
   (TeX-add-symbols
    "documentTitle")
   (LaTeX-add-labels
    "fig:overview"
    "fig:filter_schematic"
    "fig:fsm_wr_states"
    "fig:io_ports_rtl"
    "fig:tb_phase3_annot"
    "fig:tb_isr_execution"
    "fig:tb_isr_results"
    "fig:worst_path"))
 :latex)

