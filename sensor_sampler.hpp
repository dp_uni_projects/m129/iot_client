#ifndef __SENSOR_SAMPLER__
#define __SENSOR_SAMPLER__
#include <DHTesp.h> // Library to be used to read the DHT22 sensor
#include "libs/base.h"
#include "tools.h"

#define MAX_BUFFER_SIZE 5 // Number of past measurements to remember

// Class which is responsible for getting data
// from the DHT sensor and triggering callbacks when
// measured values exceed the user-defined limits.
class SensorSampler : public Base
{
  public:
	void update_values(float *, float *);
	void initialize(uint16_t pin);
	void loop();
	void set_sampling_period(uint16_t period);
	void set_temp_high_thresh(uint8_t temp_high);
	void set_hum_high_thresh(uint8_t hum_high);
	void set_temp_high_callback(void (*func)(float *));
	void set_hum_high_callback(void (*func)(float *));
	void
	set_post_calculation_callback(void (*func)(float *a_t, float *b_t, float *a_h, float *b_h));
	void calculate_rates();
	void measure();

  private:
	DHTesp	 dht;					   // Sensor instance
	uint32_t _last_sampling_time;	   // millis
	uint16_t _sampling_period = 10000; // ms

	float _temperature_measurements[MAX_BUFFER_SIZE];
	float _humidity_measurements[MAX_BUFFER_SIZE];

	uint8_t _next_measurement_index = 0; // index of next buffer position to
	// store measurement into
	uint8_t _current_measurement_index = 0; // index of last stored measurement

	uint8_t _temperature_threshold_high = 30;
	uint8_t _humidity_threshold_high = 60;

	// Callbacks
	void (*_temp_high_callback)(float *temp);
	void (*_hum_high_callback)(float *hum);
	void (*_post_calculation_callback)(float *a_t, float *b_t, float *a_h, float *b_h);

	bool _is_buffer_full = false;
};

void SensorSampler::set_temp_high_callback(void (*func)(float *) = NULL)
{
	if(func != NULL)
	{
		_temp_high_callback = func;
	}
}
void SensorSampler::set_hum_high_callback(void (*func)(float *) = NULL)
{
	if(func != NULL)
	{
		_hum_high_callback = func;
	}
}

void SensorSampler::set_post_calculation_callback(
	void (*func)(float *a_t, float *b_t, float *a_h, float *b_h))
{
	if(func != NULL)
	{
		_post_calculation_callback = func;
	}
}

void SensorSampler::set_sampling_period(uint16_t period)
{
	debug.print(F("New sampling period="));
	debug.println(period);
	_sampling_period = period;
}

void SensorSampler::set_temp_high_thresh(uint8_t temp_high)
{
	debug.print(F("New high temp threshold="));
	debug.println(temp_high);
	_temperature_threshold_high = temp_high;
}

void SensorSampler::set_hum_high_thresh(uint8_t hum_high)
{
	debug.print(F("New high hum threshold="));
	debug.println(hum_high);
	_humidity_threshold_high = hum_high;
}

// Initialization function
void SensorSampler::initialize(uint16_t pin)
{
	debug.println(F("Initializing DHT22"));
	dht.setup(pin, DHTesp::DHT22);
	disable(); // Disable measuring until we have received
			   // acknowledgment from the server
}

// Function that gets data from the sensor
// and inserts it to the buffer of measurements
void SensorSampler::measure()
{
	debug.println(F("Updating DHT22 sensor data"));
	float _humidity = dht.getHumidity();
	float _temperature = dht.getTemperature();
	debug.print(F("T="));
	debug.print(_temperature);
	debug.print(F("oC\tRH="));
	debug.print(_humidity);
	debug.println(F("%"));

	// Store the values in the buffer
	_temperature_measurements[_next_measurement_index] = _temperature;
	_humidity_measurements[_next_measurement_index] = _humidity;

	// Remember the index of the current measurement
	_current_measurement_index = _next_measurement_index;
	// Buffer is circular, the index wraps around after exceeding MAX_BUFFER_SIZE
	_next_measurement_index = (_next_measurement_index + 1) % MAX_BUFFER_SIZE;

	// Simple workaround to determine if buffer has filled
	// for the first time
	if(!_is_buffer_full && _current_measurement_index >= (MAX_BUFFER_SIZE - 1))
	{
		_is_buffer_full = true;
		debug.println(F("Data buffer is now full"));
	}

	//
	if(_is_buffer_full)
	{
		calculate_rates();
	}
	_last_sampling_time = millis();
}

void SensorSampler::calculate_rates()
{
	// Index of oldest measurement in the array.
	// Remember that the buffer is circular.
	int oldest_meas_ind = _next_measurement_index;

	// Sort temps and hums by time acquired
	// Preferred a simple for loop instead of something fancier
	float sorted_temps[MAX_BUFFER_SIZE];
	float sorted_hums[MAX_BUFFER_SIZE];
	float x_axis[MAX_BUFFER_SIZE];

	// TODO this does not run when oldest_meas_ind = 0?
	for(int i = oldest_meas_ind; i < (oldest_meas_ind + MAX_BUFFER_SIZE); i++)
	{
		sorted_temps[i - oldest_meas_ind] = _temperature_measurements[i % MAX_BUFFER_SIZE];
		sorted_hums[i - oldest_meas_ind] = _humidity_measurements[i % MAX_BUFFER_SIZE];
		x_axis[i - oldest_meas_ind] = (float)(i - oldest_meas_ind);
	}

	float a1, b1, a2, b2;
	linear_least_squares(x_axis, sorted_temps, MAX_BUFFER_SIZE, &a1, &b1);
	linear_least_squares(x_axis, sorted_hums, MAX_BUFFER_SIZE, &a2, &b2);

	if(_post_calculation_callback != NULL)
	{
		_post_calculation_callback(&a1, &b1, &a2, &b2);
	}
}

// Loop function to be called from the main loop
void SensorSampler::loop()
{
	// Do not measure if disabled
	if(is_enabled())
	{
		// Measure at user-specified intervals
		if(millis() - _last_sampling_time >= _sampling_period)
		{
			measure();

			// Check Temp limits
			if(_temperature_measurements[_current_measurement_index] >= _temperature_threshold_high)
			{
				if(_temp_high_callback != NULL)
				{
					debug.print(F("High temperature ("));
					debug.print(_temperature_measurements[_current_measurement_index]);
					debug.println(F("oC), running high temp callback"));
					_temp_high_callback(&_temperature_measurements[_current_measurement_index]);
				}
			}

			// Check Humidity limits
			if(_humidity_measurements[_current_measurement_index] >= _humidity_threshold_high)
			{
				if(_hum_high_callback != NULL)
				{
					debug.print(F("High humidity ("));
					debug.print(_humidity_measurements[_current_measurement_index]);
					debug.println(F("%), running high humidity callback"));
					_hum_high_callback(&_humidity_measurements[_current_measurement_index]);
				}
			}
		}
	}
}

#endif
