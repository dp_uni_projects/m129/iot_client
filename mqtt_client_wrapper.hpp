#ifndef __MQTT_CLIENT_WRAPPER__
#define __MQTT_CLIENT_WRAPPER__
#include <PubSubClient.h>
#ifdef ESP8266
#include <ESP8266WiFi.h>
#else
#include <WiFi.h>
#endif
#include "libs/base.h"

#define SERVER_ADDRESS_LENGTH 40
#define USERNAME_LENGTH		  10
#define PASSWORD_LENGTH		  15
#define CLIENT_ID_LENGTH	  10
#define MAX_TOPICS			  5
#define MAX_TOPIC_LENGTH	  30
#define MAX_ENDPOINT_LENGTH	  10

// A struct which encapsulates a Mqtt topic with
// a function pointer to a callback to run
// when a message arrives at the specific
// topic
typedef struct _MqttTopicSubscription
{
	char topic[MAX_TOPIC_LENGTH];
	void (*callback)(char *message, uint16_t length);
} MqttTopicSubscription;

// Class which abstracts the WiFi and PubSubClient
// so that a more clean and simple interface is provided
// to the developer.
class MqttClientWrapper : public Base
{
  public:
	void set_mqtt_client_id(const char *username);		 // MQTT username
	void set_server_address(const char *server_address); // MQTT server address
	void set_server_username(const char *username);		 // MQTT server username
	void set_server_password(const char *password);		 // MQTT server username
	void set_mqtt_callback(void (*func)(const char *topic, byte *payload, unsigned int length));
	void set_reconnection_callback(void (*func)(void));
	bool subscribe_to(char *topic,
					  void (*callback)(char *,
									   uint16_t)); // Subscribe to a MQTT topic and setup a callback
												   // to run when receiving data on this topic
	bool publish_to(char *topic, char *data);	   // Publish message to a specific topic
	void loop();								   // Function to run in main loop
	void initialize(char *		ssid,
					const char *pw,
					char *		server_address,
					char *		username); // Connect to WiFi and MQTT server
	void
	run_appropriate_callback(const char * topic,
							 byte *		  payload,
							 unsigned int length); // Method to call from the main MQTT callback

  private:
	WiFiClient			  wifi_client;
	PubSubClient		  client;
	MqttTopicSubscription _topics[MAX_TOPICS];
	char				  _client_id[CLIENT_ID_LENGTH];
	char				  _server_username[USERNAME_LENGTH];
	char				  _server_password[PASSWORD_LENGTH];
	char				  _ssid[30];
	char				  _pw[40];
	// char				  _server[SERVER_ADDRESS_LENGTH];
	char _server_address[SERVER_ADDRESS_LENGTH];

	uint8_t _num_topics;

	bool _mqtt_connect();
	bool _wifi_connect();
	void _clear_topics();
	void _subscribe_to_topics();

	void (*_reconnection_callback)(void);
};

void MqttClientWrapper::initialize(char *ssid, const char *pw, char *server_address, char *username)
{
	strncpy(_pw, pw, 40);
	strncpy(_ssid, ssid, 30);
	strncpy(_server_address, server_address, 30);
	strncpy(_client_id, username, CLIENT_ID_LENGTH);

	_wifi_connect();

	client.setClient(wifi_client);
	client.setServer(_server_address, 1883);

	_mqtt_connect();
}

// Function that configures the main callback for the PubSubClient.
void MqttClientWrapper::set_mqtt_callback(
	void (*func)(const char *topic, byte *payload, unsigned int length) = NULL)
{
	if(func != NULL)
	{
		client.setCallback(func);
	}
}

void MqttClientWrapper::set_reconnection_callback(void (*func)(void) = NULL)
{
	if(func != NULL)
	{
		_reconnection_callback = func;
	}
}

// Function that loops across all configured
// topics and subscribes to them with QoS=1
void MqttClientWrapper::_subscribe_to_topics()
{
	for(int i = 0; i < _num_topics; i++)
	{
		if(_topics[i].topic[0] != '\0')
		{
			if(client.subscribe(_topics[i].topic, 1))
			{
				debug.print(F("Successfully subscribed to \""));
			}
			else
			{
				debug.print(F("Failed to subscribe to \""));
			}
			debug.print(_topics[i].topic);
			debug.println(F("\""));
		}
	}
}

bool MqttClientWrapper::_wifi_connect()
{
	if(WiFi.status() == WL_CONNECTED)
	{
		debug.println(F("Already connected to Wifi"));
		return true;
	}
	debug.print(F("Connecting to Wifi "));
	debug.println(_ssid);

	WiFi.begin(_ssid, _pw);
#ifdef ESP8266
	while(WiFi.status() != WL_CONNECTED)
#else
	while(WiFi.waitForConnectResult() != WL_CONNECTED)
#endif
	{
		delay(500); // Required, else crashes
	}

	debug.print(F("Connected, IP is "));
	debug.println(WiFi.localIP().toString().c_str());
}

bool MqttClientWrapper::_mqtt_connect()
{
	if(client.connected())
	{
		debug.println(F("Already connected to MQTT broker"));
		return true;
	}

	// Hardcoded for now
	// if(client.connect(_client_id, "samos", "karlovasi33#"))
	if(client.connect(_client_id, _server_username, _server_password))
	{
		_subscribe_to_topics();
		client.loop();
		debug.print(F("Successfully connected to MQTT broker "));
		debug.println(_server_address);
		return true;
	}
	return false;
}

// Loop to call from main code loop
// which takes care of MQTT and Wifi
// disconnections
void MqttClientWrapper::loop()
{
	client.loop(); // PubSubClient keepalive
	if(!client.connected())
	{
		debug.print(F("Disconnected from MQTT server, reason: "));
		debug.println((int16_t)client.state());
		_mqtt_connect(); // Reconnect if disconnected
		if(client.connected() && _reconnection_callback != NULL)
		{
			_reconnection_callback();
		}
	}

	// Reconnect to WiFi if not connected
	if(WiFi.status() != WL_CONNECTED)
	{
		_wifi_connect(); // Reconnect if disconnected
	}
}

// Set the username for the MQTT client
void MqttClientWrapper::set_mqtt_client_id(const char *client_id)
{
	strncpy(_client_id, client_id, CLIENT_ID_LENGTH);
}

void MqttClientWrapper::set_server_username(const char *username)
{
	strncpy(_server_username, username, USERNAME_LENGTH);
}

void MqttClientWrapper::set_server_password(const char *password)
{
	strncpy(_server_password, password, PASSWORD_LENGTH);
}

// Set the MQTT server's address
void MqttClientWrapper::set_server_address(const char *server_address)
{
	strncpy(_server_address, server_address, SERVER_ADDRESS_LENGTH);
}

bool MqttClientWrapper::publish_to(char *topic, char *data)
{
	if(client.connected())
	{
		client.publish(topic, data);
	}
}

void MqttClientWrapper::_clear_topics()
{
	debug.println(F("Clearing configured topics"));
	for(int i; i < MAX_TOPICS; i++)
	{
		_topics[i].topic[0] = '\0';
	}
	_num_topics = 0;
}

bool MqttClientWrapper::subscribe_to(char *topic, void (*callback)(char *, uint16_t) = NULL)
{
	if(_num_topics < MAX_TOPICS - 1)
	{
		MqttTopicSubscription _new_topic;
		strncpy(_new_topic.topic, topic, MAX_TOPIC_LENGTH);
		_new_topic.callback = callback;
		_topics[_num_topics] = _new_topic;
		_num_topics += 1;
		return true;
	}
	return false;
}

// Function to be called from PubSubClient callback, which
// checks the topic that the new MQTT message was received on
// and checks to see if the user has configured a callback
// specifically for that.
void MqttClientWrapper::run_appropriate_callback(const char * topic,
												 byte *		  payload,
												 unsigned int length)
{
	for(int i; i < _num_topics; i++)
	{
		if(strcmp(topic, _topics[i].topic) == 0)
		{
			debug.print(F("Found callback for "));
			debug.println(topic);
			if(_topics[i].callback != NULL)
			{
				char temp[length + 1];
				strncpy(temp, (char *)payload, length);
				temp[length] = '\0';
				_topics[i].callback(temp, length);
			}
		}
	}
}

#endif
