// #ifndef ESP8266
// #error This firmware only works for ESP8266
// #endif
#include "tools.h"
#include "mqtt_client_wrapper.hpp"
#include "sensor_sampler.hpp"

#define DHT_DATA_PIN 2
#define GENERATOR	 3
#define VERSION		 (F("D1 mini IoT client"))

MqttClientWrapper mqtt_wrapper; // MQTT wrapper
SensorSampler	  sensor;

// WiFi connection parameters (WPA security protocol)
char *		WIFI_SSID = "Einai_ayth_ta_kanei";
const char *WIFI_PASSWORD = "!TiAsCp?YES96";

// Callback for topic user3/data
void topic_data_callback(char *data, uint16_t len)
{
	char temp[3] = "";
	strncpy(temp, data, 2);
	uint8_t sampling_period = atoi(temp);

	strncpy(temp, data + 2, 2);
	uint8_t temp_thresh = atoi(temp);
	strncpy(temp, data + 4, 2);
	uint8_t rh_thresh = atoi(temp);

	Serial.print(F("Sampling period="));
	Serial.print(sampling_period);
	Serial.print(F("\tT_thresh="));
	Serial.print(temp_thresh);
	Serial.print(F("\tRH_thresh="));
	Serial.println(rh_thresh);

	uint8_t sp_crc = crc_8(3, &sampling_period, 1);
	uint8_t tt_crc = crc_8(3, &temp_thresh, 1);
	uint8_t rt_crc = crc_8(3, &rh_thresh, 1);

	Serial.print(F("SP CRC="));
	Serial.print(sp_crc);
	Serial.print(F("\tTT CRC="));
	Serial.print(tt_crc);
	Serial.print(F("\tRT CRC="));
	Serial.println(rt_crc);

	sensor.set_temp_high_thresh(temp_thresh);
	sensor.set_hum_high_thresh(rh_thresh);
	sensor.set_sampling_period(sampling_period * 1000);
	// sensor.set_sampling_period(1000);

	char crc_string[10];
	snprintf(crc_string, 10, "%03d%03d%03d", sp_crc, tt_crc, rt_crc);
	mqtt_wrapper.publish_to("user3/dataCrc", crc_string);
}

// Callback for topic user3/control
void topic_control_callback(char *data, uint16_t len)
{
	if(strcmp(data, "startMeasurements") == 0)
	{
		Serial.println(F("CRC values verified by server"));
		sensor.enable();
	}
	else if(strcmp(data, "updateConfig") == 0)
	{
		request_config_from_server();
	}
	else if(strcmp(data, "crcError") == 0)
	{
		Serial.println(F("CRC calculation error"));
		request_config_from_server();
		sensor.disable();
	}
}

// void topic_hello_callback(char *data, uint16_t len) { Serial.println((char *)data); }
// void topic_esp32_callback(char *data, uint16_t len) { Serial.println((char *)data); }

void print_rates(float *a_t, float *b_t, float *a_h, float *b_h)
{
	Serial.println(F("-----------------------"));
	Serial.println(F("\nLinear fit coefficients"));
	Serial.println(F("-----------------------"));
	Serial.println(F("Variable\ta\tb"));
	Serial.print(F("Temperature\t"));
	Serial.print(*a_t, 2);
	Serial.print(F("\t"));
	Serial.println(*b_t, 2);
	Serial.print(F("Humidity\t"));
	Serial.print(*a_h, 2);
	Serial.print(F("\t"));
	Serial.println(*b_h, 2);
	Serial.println(F("-----------------------"));
}

void high_temp_alert(float *temp)
{
	Serial.print(F("Temperature High ("));
	Serial.print(*temp);
	Serial.println(F("oC)"));
}

void high_hum_alert(float *hum)
{
	Serial.print(F("Humidity high ("));
	Serial.print(*hum);
	Serial.println(F("%)"));
}

// Callback which is run by PubSubClient whenever any new
// message on any new topic arrives
void mqtt_callback(const char *topic, byte *payload, unsigned int length)
{
	char temp[length + 1];
	strncpy(temp, (char *)payload, length);
	temp[length] = '\0';
	Serial.print(F("Received \""));
	Serial.print(temp);
	Serial.print(F("\" on topic \""));
	Serial.print(topic);
	Serial.println(F("\""));
	// Run the appropriate user-configured callback for the topic
	mqtt_wrapper.run_appropriate_callback(topic, payload, length);
}

void request_config_from_server() { mqtt_wrapper.publish_to("user3/control", "sendConfig"); }

void setup()
{
	Serial.begin(115200);
	Serial.println(VERSION);
	delay(1000);

	// For testing only
	mqtt_wrapper.enable_debugging(&Serial);
	sensor.enable_debugging(&Serial);

	// Intialization and configuration
	sensor.initialize(DHT_DATA_PIN); // DHT data pin number
	sensor.set_temp_high_callback(high_temp_alert);
	sensor.set_hum_high_callback(high_hum_alert);
	sensor.set_post_calculation_callback(print_rates);
	mqtt_wrapper.set_server_username("samos");
	mqtt_wrapper.set_server_password("karlovasi33#");
	mqtt_wrapper.set_reconnection_callback(request_config_from_server);
	mqtt_wrapper.set_mqtt_callback(mqtt_callback);
	// mqtt_wrapper.subscribe_to("user3/server", topic_hello_callback);
	// mqtt_wrapper.subscribe_to("user3/esp32", topic_esp32_callback);

	Serial.println(
		F("Connecting to Wifi, MQTT server and Subscribing to topics /data and /control"));
	mqtt_wrapper.subscribe_to("user3/data", topic_data_callback);
	mqtt_wrapper.subscribe_to("user3/control", topic_control_callback);
	mqtt_wrapper.initialize(WIFI_SSID, WIFI_PASSWORD, "esp-32.zapto.org", "user3");

	// Request configuration from server
	Serial.println(F("Requesting configuration from server"));
	request_config_from_server();
}

void loop()
{
	mqtt_wrapper.loop();
	sensor.loop();
}
