#ifndef __TOOLS__
#define __TOOLS__
// Byte array CRC-8 calculator.
// A generator number, a byte array
// and its length must be provided.
uint8_t crc_8(uint8_t generator, uint8_t *array, uint8_t num_bytes)
{
	uint8_t crc = 0;

	for(uint8_t i = 0; i < num_bytes; i++)
	{
		crc ^= array[i];
		for(uint16_t j = 0; j < 8 * sizeof(uint8_t); j++)
		{
			if((crc & 0x80) != 0)
			{
				crc = (uint8_t)((crc << 1) ^ generator);
			}
			else
			{
				crc <<= 1;
			}
		}
	}
	return crc;
}

// Function that calculates the best-fitting
// line to the x-y data supplied, via the method
// of linear least squares.
void linear_least_squares(float *x, float *y, uint8_t length, float *a, float *b)
{
	float sum_x, sum_y, sum_xy, sum_xx;
	sum_x = sum_y = sum_xx = sum_xy = 0;

	for(int i = 0; i < length; i++)
	{
		sum_x += x[i];
		sum_y += y[i];
		sum_xy += x[i] * y[i];
		sum_xx += x[i] * x[i];
	}

	*a = (sum_x * sum_y - length * sum_xy) / (sum_x * sum_x - length * sum_xx);
	*b = (sum_y - *a * sum_x) / length;
}

#endif
